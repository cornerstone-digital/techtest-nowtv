'use strict';

angular
  .module('icebergMovies')
  .factory('MovieService', MovieService)

MovieService.$inject = ['$log', 'ResourceWithPaging'];

function MovieService($log, ResourceWithPaging) {
  return {
    getList: function(params) {
      params = params != null ? params : {};

      var result = ResourceWithPaging.one('movies').get(params);

      return result;
    }
  };
}
